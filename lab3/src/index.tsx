import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom';
import App from './App';
//import CalendarPage from './components/CalendarPage';

ReactDOM.render(
  <BrowserRouter>
  <App></App>
  </BrowserRouter>,
document.getElementById('root')
);


import React from "react";
import TextField from "@mui/material/TextField";
import { Grid, Link, Typography, Paper, Button } from "@mui/material";
import UserImage from "../UserImage.png";

function SignUp() {
  const btnstyle = { margin: "8px 0" };
  document.body.style.background = "linear-gradient(to right, #FC466B, #1975d2)";

  return (
    <Grid
      container
      direction="column"
      sx={{
        border: "0px",
        width: "auto",
        height: "auto",
        paddingRight: "auto",
        paddingBottom: "auto",
      }}
    >
      <Grid
        container
        direction="row"
        borderRadius="50px"
        bgcolor="white"
        sx={{
          border: "0px",
          height: "auto",
          paddingBottom: "auto",
        }}
        p="5%"
        marginTop="8%"
        marginLeft="12%"
        marginBottom="10%"
        width="75%"
      >
        <Grid
          container
          direction="column"
          xs={12}
          sm={6}
          md={6}
          alignContent="flex-end"
          pr={7}
        >
          <img src={UserImage} width="250" height="250"></img>
        </Grid>
        <Grid item xs={10} sm={6} md={4} alignContent="center">
          <Paper elevation={0}>
            <Grid container direction="column" pl={9}>
              <Typography align="center" variant="h5" pb={3}>
                User Sign Up
              </Typography>
              <TextField variant="filled" label="Email id" type="email" />
              <TextField variant="filled" label="Username" type="username" />
              <TextField variant="filled" label="Password" type="password" />
              <Button
                type="submit"
                color="primary"
                variant="contained"
                style={btnstyle}
              >
                Sign up
              </Button>
              <Typography align="center">
                <Link href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
                  Forgot Username / Password?
                </Link>
              </Typography>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default SignUp;

import { Switch, Route } from 'react-router-dom';
import ImageUpload from './ImageUpload';
import Login from './Login';
import TableV2 from './TableV2';

function PublicRoute() {
  return (
    <Switch>
    <Route path='/' exact>
      <Login />
    </Route>
    <Route path='/table'>
        <TableV2/>
    </Route>
    <Route path='/imageUpload' >
        <ImageUpload />
    </Route>
  </Switch>
  )
}

export default PublicRoute;
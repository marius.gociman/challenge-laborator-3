import React, { useEffect, useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import moment from 'moment';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import "./calendarStyle.css";
import { Button } from '@mui/material';
import { cpuUsage } from 'process';

function CustomCalendar() {
  const [start, setStart] = useState(new Date())
  const [end, setEnd] = useState(new Date())

  const changeDate = (e: any) => {
    setStart(e[0])
    setEnd(e[1]);
  }

  const [tableData, setTableData] = useState<{}[]>([]);

  const columns: GridColDef[] = [
    { field: 'start_date', headerName: 'Start Date', width: 250 },
    { field: 'end_date', headerName: 'End Date', width: 250 },
  ];

  let id = 0;

  const handleID = () => {
    return id++;
  }

  let body = {
    "start_date": moment(start).format("YYYY-MM-DD"),
    "end_date": moment(end).format("YYYY-MM-DD"),
  };

  const AddRow = () => {
    var temp = [...tableData, body];
    setTableData(temp);
    AddSchedule(body);
  }

  const RemoveRow = () => {
    var temp = [...tableData];
    setTableData(temp);
  }

  const AddSchedule = (body: {}) => {
    console.log("fetching")
    fetch(
      "https://us-central1-causal-hour-334314.cloudfunctions.net/createSchedule",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body)
      }
    )
      .then((response) => response.json())
      .catch((err) => {
        console.log(err.message);
        RemoveRow();
      });
  }

  useEffect(
    () => {
      fetch('https://us-central1-causal-hour-334314.cloudfunctions.net/getSchedules', {
        method: 'GET',
      }).then((response) => response.json())
        .then((data) => {
          setTableData(data)
        }).catch(err => console.log(err.message))
    },
    []
  )

  return (
    <>
      <Calendar
        value={[start, end]}
        onChange={changeDate}
        className={"calendar"}
        selectRange={true}
      />
      <p>
        Current start date is <b>{moment(start).format("YYYY-MM-DD")}</b>
      </p>
      <p>
        Current end date is <b>{moment(end).format("YYYY-MM-DD")}</b>
      </p>

      <Button onClick={AddRow} variant="contained">
        Add Schedule
      </Button>

      <div style={{ height: 500, width: 508 }}>
        <DataGrid
          rows={tableData}
          columns={columns}
          pageSize={50}
          getRowId={handleID}
        />
      </div>
    </>
  );
}

export default CustomCalendar;

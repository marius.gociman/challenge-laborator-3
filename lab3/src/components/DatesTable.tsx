import * as React from 'react';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import {useEffect, useState} from "react";

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 200 },
  { field: 'name', headerName: 'Name', width: 200 },
  { field: 'size', headerName: 'Size', width: 200 },
  { field: 'result', headerName: 'Result', width: 200 },
];

export default function DateTable() {
    const [tableData,setTableData]=useState([]);
    useEffect(
        () => {
            fetch('http://localhost:3001/results', {
            method: 'GET',
        }).then((response) => response.json())
                .then((data) => {
                    setTableData(data)
                }).catch(err => console.log(err.message))},
        []
    )
  return (
    <div style={{ height: 800, width: 808 }}>
      <DataGrid
        rows={tableData}
        columns={columns}
        pageSize={50}
      />
    </div>
  );
}
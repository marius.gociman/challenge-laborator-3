import { Switch, Route } from 'react-router-dom';
import Login from './Login';

function PrivateRoute() {
  return (
    <Switch>
        <Route path='/' exact>
        <Login />
        </Route>
    </Switch>
  )
}

export default PrivateRoute;
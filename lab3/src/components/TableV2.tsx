import * as React from "react";
import { DataGrid, GridColumns } from "@mui/x-data-grid";
import { Link} from "@mui/material";

const columns: GridColumns = [
  {
    field: "id",
    headerName: "Image name",
    headerAlign: "center",
    width: 170,
  },
  {
    field: "size",
    headerName: "Size",
    headerAlign: "center",
    type: "number",
    width: 130,
    editable: false,
  },
  {
    field: "recognition",
    headerName: "Recognition result",
    headerAlign: "center",
    width: 200,
    editable: false,
  },
  {
    field: "image_download",
    headerName: "Image download link",
    headerAlign: "center",
    width: 230,
    editable: false,
    renderCell: (params) => (
      <Link href={`${params.value}`}>{params.id}</Link>
    )
  },
];

const rows = [
  {
    id: "Landscape 1",
    size: 159,
    recognition: "6.0",
    image_download: "https://pixabay.com/photos/landscapes-forest-nature-fall-mood-2887796/"
    ,
  },
  {
    id: "Landscape 2",
    size: 237,
    recognition: "9.0",
    image_download: "https://www.flickr.com/photos/giuseppemilo/19954826630",
  },
  { id: "Sea 1", size: 262, recognition: "16.0", image_download: "https://ro.pinterest.com/pin/17451517276057877/" },
  { id: "Sea 2", size: 305, recognition: "3.7", image_download: "https://www.publicdomainpictures.net/ro/view-image.php?image=4316&picture=bench-i-sea" },
  { id: "Putin  ", size: 356, recognition: "16.0", image_download: "https://i.pinimg.com/originals/f8/aa/75/f8aa75f0b3e77df4f9386f633a237f46.jpg" },
  { id: "Juan", size: 375, recognition: "0.0", image_download: "https://i.ytimg.com/vi/H9aC5AGY9YU/sddefault.jpg" },
  { id: "Rick Astley", size: 392, recognition: "0.2", image_download: "https://variety.com/wp-content/uploads/2021/07/Rick-Astley-Never-Gonna-Give-You-Up.png?w=1024" },
  { id: "Big Smoke", size: 408, recognition: "3.2", image_download: "https://i1.sndcdn.com/artworks-fBvQUKQfO8Kervzy-PnlM0g-t500x500.jpg" },
  { id: "Elmo's Hell", size: 452, recognition: "25.0", image_download: "https://pbs.twimg.com/media/EdrEf_hVAAE5KGs.jpg" },
  { id: "Mint KitKat", size: 518, recognition: "26.0", image_download: "https://candyland.ro/image/cache/catalog/journal3/CL%20poze%20noi/Kit%20Kat%20Duos%20Dark%20Choc%20Mint%20Bar%20King%20Size%2085g-714x714.png" },
];

export default function DataGridDemo() {
  return (
    <div style={{ height: 650, width: "100%" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        checkboxSelection
        disableSelectionOnClick
      />
    </div>
  );
}

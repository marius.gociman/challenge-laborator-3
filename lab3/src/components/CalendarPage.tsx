import CustomCalendar from "./CustomCalendar";

function CalendarPage()
{
    return(
        <div>
            <h1>Date Picker</h1>
            <CustomCalendar/>
        </div>
    );
}
export default CalendarPage
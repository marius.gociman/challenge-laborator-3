import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';

const columns = [
  { field: 'id', headerName: 'Dessert(100g serving)', width: 170 },
  {
    field: 'calories',
    headerName: 'Calories',
    type: 'number',
    width: 90,
    editable: true,
  },
  {
    field: 'fat',
    headerName: 'Fat(g)',
    type: 'number',
    width: 70,
    editable: true,
  },
  {
    field: 'carbs',
    headerName: 'Carbs(g)',
    type: 'number',
    width: 90,
    editable: true,
  },
  {
    field: 'protein',
    headerName: 'Protein(g)',
    type: 'number',
    width: 100,
    editable: true,
  },
  {
    field: 'sodium',
    headerName: 'Sodium(mg)',
    type: 'number',
    width: 120,
    editable: true,
  },
  {
    field: 'calcium',
    headerName: 'Calcium(%)',
    type: 'number',
    width: 110,
    editable: true,
  },
  {
    field: 'iron',
    headerName: 'Iron(%)',
    type: 'number',
    width: 80,
    editable: true,
  },
 
];

const rows = [
  { id: "Frozen yogurt", calories: 159, fat: 6.0, carbs: 24, protein: 4.0, sodium: 87, calcium: 14, iron: 1 },
  { id: "Ice cream sandwich", calories: 237, fat: 9.0, carbs: 37, protein: 4.3, sodium: 129, calcium: 8, iron: 1 },
  { id: "Eclair", calories: 262, fat: 16.0, carbs: 24, protein: 6.0, sodium: 337, calcium: 6, iron: 7 },
  { id: "Cupcake", calories: 305, fat: 3.7, carbs: 67, protein: 4.3, sodium: 413, calcium: 3, iron: 8 },
  { id: "Gingerbread", calories: 356, fat: 16.0, carbs: 49, protein: 3.9, sodium: 327, calcium: 7, iron: 16 },
  { id: "Jelly bean", calories: 375, fat: 0.0, carbs: 94, protein: 0.0, sodium: 50, calcium: 0, iron: 0 },
  { id: "Lollipop", calories: 392, fat: 0.2, carbs: 98, protein: 0, sodium: 38, calcium: 0, iron: 2 },
  { id: "Honeycorn", calories: 408, fat: 3.2, carbs: 87, protein: 6.5, sodium: 562, calcium: 0, iron: 45 },
  { id: "Donut", calories: 452, fat: 25.0, carbs: 51, protein: 4.9, sodium: 326, calcium: 2, iron: 22 },
  { id: "KitKat", calories: 518, fat: 26.0, carbs: 65, protein: 7, sodium: 54, calcium: 12, iron: 6 },
];

export default function DataGridDemo() {
  return (
    <div style={{ height: 650, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
        checkboxSelection
        disableSelectionOnClick
      />
    </div>
  );
}

import { Button } from "@mui/material";
import { useState } from "react";

function ImageUpload() {
  const initialImg = "https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty-300x240.jpg";
  const [shownImage, setShownImage] = useState(initialImg);
  const [toSendImage, setToSendImage] = useState<File>();
  const [isImage, setIsImage] = useState(false);
  const redirect = '/results';

  var validated = false;
  var _URL = window.URL || window.webkitURL;

  function onSendClick() {
    const body = new FormData();
    if(toSendImage != null){
        body.append("file", toSendImage);
    }
    fetch("http://localhost:3001/evaluate", {
      method: "POST",
      body: body,
    })
      .then((response) => response.json())
      .then((data) => {
        fetch('http://localhost:3001/addResult', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "name": toSendImage?.name,
                "result": data[0],
                "size": toSendImage?.size
            }),
            }).catch(err => console.log(err.message)).then(() => {window.location.replace(redirect)});
        }
      ).catch((err) => console.log(err.message));
  }

  return (
    <div>
      <div>
        <Button variant="contained" component="label">
          Upload File
          <input
            type="file"
            hidden
            name="file"
            onChange={(event) => {
              if (event.target.files && event.target.files[0]) {
                var temp = _URL.createObjectURL(event.target.files[0]);
                var img = new Image();
                img.onload = function () {
                  if (img.width !== 28 || img.height !== 28) {
                    _URL.revokeObjectURL(temp);
                    temp = initialImg;
                    validated = false;
                    alert("Image is not 28 x 28.");
                  } else {
                    validated = true;
                  }
                  setShownImage(temp);
                  if(event.target.files != null) {
                    setToSendImage(event.target.files[0]);
                    }
                  setIsImage(validated);
                };
                img.src = temp;
              }
            }}
          />
        </Button>
        <span> </span>
        <Button variant="contained" component="label" disabled={!isImage} onClick={onSendClick}>
            Send Image
        </Button>
      </div>

      <div>
        <img src={shownImage} style={{ maxWidth: "100%" }} />
      </div>
    </div>
  );
}

export default ImageUpload;

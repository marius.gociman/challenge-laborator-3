import React from 'react';
import ImageUpload from './components/ImageUpload';
import ResultTable from './components/ResultTable';
import {Route, BrowserRouter, Switch} from "react-router-dom";
import CalendarPage from './components/CalendarPage';

function App() {
  return (
  <BrowserRouter>
    <Switch>
      <Route exact path="/"> <ImageUpload></ImageUpload></Route>
      <Route exact path="/results"> <ResultTable></ResultTable></Route>
      <Route exact path="/calendar"> <CalendarPage></CalendarPage></Route>
    </Switch>
  </BrowserRouter>
    )
}
export default App